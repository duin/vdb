#!/usr/bin/env node
import path from 'node:path';
import DB from '../lib/Vdb.js'

const REINDEX = false;

/**
* @param {string} line
* @param {number} index - line number
* @returns {string}
*/
const filterUnitsLine = (line, index) => {
  if (index === 0) return;
  const bits = line.split('\t');
  if (bits.length > 2) {
    return `${bits[0]}\t${bits[2]}\t${bits[3]||''}`;
  }
  return;
}

const STORAGE = path.resolve('storage') ;
const db = new DB(STORAGE, 'MLE5Large');
if (REINDEX || !db.list().includes('units')) {
  const file = path.resolve('scenarios/units.tsv');
  await db.create(file, 'units', filterUnitsLine);
  console.log('units created');
}
let str = await db.search('units', 'stuks', {treshhold:0.50,  results:10, preRead: 0, postRead: 0});
console.log('-- stuks --');
console.log(str);
const result = await db.getResult('units', 'piece', 5);
console.log('-- piece --');
console.log(result);
