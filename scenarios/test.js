#!/usr/bin/env node
import path from 'node:path';
import DB from '../lib/Vdb.js'

const REINDEX = true;

const STORAGE = path.resolve('storage') ;
const db = new DB(STORAGE, 'MLE5Large');
if (REINDEX || !db.list().includes('readme')) {
  const file = path.resolve('README.md');
  await db.create(file, 'readme');
  console.log('readme created');
}
let str = await db.search('readme', 'Create a database', {treshhold:0.86,  results:4, preRead: 3, postRead: 2});
console.log(str);
