export { Vdb as default };
export type VResult = {
    /**
     * - The dabase line
     */
    idx: number;
    /**
     * - The higher the better (value between 0 -1)
     */
    similarity: number;
    /**
     * -  The retrieved text line from the file
     */
    text: string;
};
export type VResultRaw = {
    /**
     * - The dabase line
     */
    idx: number;
    /**
     * - The higher the better (value between 0 -1)
     */
    similarity: number;
};
export type VSelector = {
    /**
     * - the number of results to search
     */
    results: number;
    /**
     * - The number of lines to return before the found index
     */
    preRead: number;
    /**
     * - The number of lines to return after the found index
     */
    postRead: number;
    /**
     * - FLoat between (0 - 1). Only pass results equal or higher then treshhold
     */
    treshhold: number;
};
/**
 * Simple vector database class.
 */
declare class Vdb {
    /**
     * Constructor for Vdb.
     * @param {string} storagePath - Path to storage folder.
     * @param {string} [model] - model to use
     */
    constructor(storagePath: string, model?: string);
    /**
    * List of available models
    */
    get models(): string[];
    /**
     * Get a list of available databases.
     * @returns {string[]} List of database names.
     */
    list(): string[];
    /**
     * Delete a database.
     * @param {string} dbName - Name of the database to delete.
     */
    delete(dbName: string): void;
    /**
     * Create or overwrite an embeddings database from a text document.
     * @param {string} file - Path to the text document.
     * @param {string} dbName - Name of the database.
     * @param {function(string):string} [filter]
     * @param {number} [batchSize=256] - Batch size for processing.
     */
    create(file: string, dbName: string, filter?: (arg0: string) => string, batchSize?: number): Promise<void>;
    /**
     * Search the database.
     * @param {string} dbName - Name of the database.
     * @param {string} query - Search query.
     * @param {VSelector} [selector] - Selector options.
     * @returns {Promise<string>} Formatted search results.
     */
    search(dbName: string, query: string, selector?: VSelector): Promise<string>;
    /**
     * Get raw search results from the database.
     * @param {string} dbName - Name of the database.
     * @param {string} query - Search query.
     * @param {number} [results=5] - Number of results to return.
     * @returns {Promise<VResult[]>} Array of results.
     */
    getResult(dbName: string, query: string, results?: number): Promise<VResult[]>;
    #private;
}
